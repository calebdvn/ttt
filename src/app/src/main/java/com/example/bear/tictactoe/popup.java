package com.example.bear.tictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class popup extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedstate){
        super.onCreate(savedstate);

        setContentView(R.layout.popup);

        Intent popupintent = getIntent();
        int winner = popupintent.getIntExtra("winner",-3);

        TextView popupMessage = (TextView) findViewById(R.id.WinnerMessage);
        if(winner==0) {
            popupMessage.setText("The cat wins!");
        }else{
            popupMessage.setText("Player "+Integer.toString(winner)+" wins!");
        }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(0.8*width),(int)(0.15*height));
    }
}
