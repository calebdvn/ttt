package com.example.bear.tictactoe;

import android.content.Intent;

class Game {

	int[][] State; // Java implicitly defaults integer arrays to zero
	int player; // 1 or -1
	int winner;
	int totalMoves = 0;

	Game(Game g) {
		State = new int[3][3];
		copyArray(g.State, this.State);
		this.player = g.player;
		this.winner = g.winner;
	}

	Game() {
		player = 1;
		State = new int[3][3];
	}

	int CheckWinner() {
		if (State[0][0] + State[0][1] + State[0][2] == 3)
			return 1;
		if (State[1][0] + State[1][1] + State[1][2] == 3)
			return 1;
		if (State[2][0] + State[2][1] + State[2][2] == 3)
			return 1;
		if (State[0][0] + State[1][0] + State[2][0] == 3)
			return 1;
		if (State[0][1] + State[1][1] + State[2][1] == 3)
			return 1;
		if (State[0][2] + State[1][2] + State[2][2] == 3)
			return 1;
		if (State[0][0] + State[1][1] + State[2][2] == 3)
			return 1;
		if (State[0][2] + State[1][1] + State[2][0] == 3)
			return 1;
		if (State[0][0] + State[0][1] + State[0][2] == -3)
			return -1;
		if (State[1][0] + State[1][1] + State[1][2] == -3)
			return -1;
		if (State[2][0] + State[2][1] + State[2][2] == -3)
			return -1;
		if (State[0][0] + State[1][0] + State[2][0] == -3)
			return -1;
		if (State[0][1] + State[1][1] + State[2][1] == -3)
			return -1;
		if (State[0][2] + State[1][2] + State[2][2] == -3)
			return -1;
		if (State[0][0] + State[1][1] + State[2][2] == -3)
			return -1;
		if (State[0][2] + State[1][1] + State[2][0] == -3)
			return -1;
		return 0; // The cat wins
	}

	void MakeMove(int position) {
		int y = position % 3;
		int x = (position - y) / 3;
		if (State[x][y] == 0) {
			State[x][y] = player;
		}
	}

	int getState(int position) {
		int y = position % 3;
		int x = (position - y) / 3;
		return State[x][y];
	}

	void copyArray(int[][] arrFrom, int[][] arrTo) {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				arrTo[i][j] = arrFrom[i][j];
	}

	int getComputerMove() {
		return minimax.mnmx(this)[1];
	}

	int resetPosition(int position) {
		int y = position % 3;
		int x = (position - y) / 3;
		State[x][y] = 0;
		if (State[x][y] != 0) {
			return 1;
		} else {
			return 0;
		}
	}

	boolean CheckGameOver() {
		if (State[0][0] != 0)
			if (State[0][1] != 0)
				if (State[0][2] != 0)
					if (State[1][0] != 0)
						if (State[1][1] != 0)
							if (State[1][2] != 0)
								if (State[2][0] != 0)
									if (State[2][1] != 0)
										if (State[2][2] != 0)
											return true;
		if (CheckWinner() != 0)
			return true;
		return false;
	}

}
