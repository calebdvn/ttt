package com.example.bear.tictactoe;

class minimax {

	static int[] mnmx(Game ttt) { // this recursive function will output the best move for current player
		Game ttt2 = new Game(ttt);
		int[] moveResults = TestAllSquares(ttt2);
		return pickTheBestMove(moveResults, ttt2.player);
	}

	private static int[] TestAllSquares(Game ttt) {
		int[] moveResults = new int[9];
		for (int testMove = 0; testMove < 9; testMove++) { // try all 9 possible moves
			if (ttt.getState(testMove) != 0) { // a mark already occupies this square
				moveResults[testMove] = 2 * (-ttt.player); // give occupied squares very negative marks
				continue; // skip this square
			}
			moveResults[testMove] = ApplyMiniMaxMark(ttt, testMove);
		}
		return moveResults;
	}

	private static int ApplyMiniMaxMark(Game ttt, int markIdx) {
		int markResult;
		int player = ttt.player;
		ttt.MakeMove(markIdx);
		if (ttt.CheckGameOver() == false) { // Have not reached end game. Call MiniMax again
			ttt.player = -player;
			markResult = mnmx(ttt)[0];
			ttt.player = player;
		} else {
			markResult = ttt.CheckWinner();
		}
		ttt.resetPosition(markIdx);
		return markResult;
	}

	private static int[] pickTheBestMove(int[] moveResults, int player) {
		// Now we find the most winning value (for minimax) and its index
		int bestVal, bestIdx;
		if (player == -1) { // If current player is player 2 then the best value is more negative
			int min = moveResults[0];
			int mini = 0;
			for (int i = 1; i < 9; i++) {
				if (min > moveResults[i]) {
					min = moveResults[i];
					mini = i;
				}
			}
			bestVal = min;
			bestIdx = mini;
		} else { // If current player is player 1, then the best value is more positive
			int max = moveResults[0];
			int maxi = 0;
			for (int i = 1; i < 9; i++) {
				if (max < moveResults[i]) {
					max = moveResults[i];
					maxi = i;
				}
			}
			bestVal = max;
			bestIdx = maxi;
		}

		int[] results = new int[2];
		results[0] = bestVal;
		results[1] = bestIdx;
		return results;
	}
}