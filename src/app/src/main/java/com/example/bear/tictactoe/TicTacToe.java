package com.example.bear.tictactoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TicTacToe extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        final Game ttt = new Game();
        int winner = 0;
        int position = 0;
        boolean gameover = false;


        final Button button0 = buttonswitch(0);
        final Button button1 = buttonswitch(1);
        final Button button2 = buttonswitch(2);
        final Button button3 = buttonswitch(3);
        final Button button4 = buttonswitch(4);
        final Button button5 = buttonswitch(5);
        final Button button6 = buttonswitch(6);
        final Button button7 = buttonswitch(7);
        final Button button8 = buttonswitch(8);

        final Button xbutt = (Button) findViewById(R.id.xbutt);
        final Button obutt  = (Button) findViewById(R.id.obutt);

        button0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 0, button0);
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 1, button1);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 2, button2);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 3, button3);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 4, button4);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 5, button5);
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 6, button6);
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 7, button7);
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tap(ttt, 8, button8);
            }
        });

        xbutt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                xbutt.setVisibility(View.INVISIBLE);
                obutt.setVisibility(View.INVISIBLE);
                ttt.player=1;
            }
        });

        obutt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                xbutt.setVisibility(View.INVISIBLE);
                obutt.setVisibility(View.INVISIBLE);

                ttt.MakeMove(4);
                buttonswitch(4).setBackgroundResource(R.drawable.x);
                ttt.player=-1;
            }
        });


    }

    void Tap(Game ttt, int position, Button butt) {

        if (ttt.getState(position)!=0)
            return;

        if (((Button) findViewById(R.id.obutt)).getVisibility() == View.VISIBLE){
            ((Button) findViewById(R.id.xbutt)).setVisibility(View.INVISIBLE);
            ((Button) findViewById(R.id.obutt)).setVisibility(View.INVISIBLE);
            ttt.player = 1;
        }

        if(ttt.player==1) {
            butt.setBackgroundResource(R.drawable.x);
        }else{
            butt.setBackgroundResource(R.drawable.o);
        }
        ttt.MakeMove(position);
        ttt.player= -ttt.player;
        if (ttt.CheckGameOver()) {
            endGame(ttt);
        } else {
            int compMove = ttt.getComputerMove();
            ttt.MakeMove(compMove);
            if(ttt.player==1) {
                buttonswitch(compMove).setBackgroundResource(R.drawable.x);
            }else {
                buttonswitch(compMove).setBackgroundResource(R.drawable.o);
            }
            ttt.player= -ttt.player;
            if (ttt.CheckGameOver()) {
                endGame(ttt);

            }

        }

    }

    Button buttonswitch(int position){
        switch (position){
            case 0: return (Button) findViewById(R.id.button_tl);
            case 1: return (Button) findViewById(R.id.button_tm);
            case 2: return (Button) findViewById(R.id.button_tr);
            case 3: return (Button) findViewById(R.id.button_ml);
            case 4: return (Button) findViewById(R.id.button_mm);
            case 5: return (Button) findViewById(R.id.button_mr);
            case 6: return (Button) findViewById(R.id.button_bl);
            case 7: return (Button) findViewById(R.id.button_bm);
            case 8: return (Button) findViewById(R.id.button_br);
            default:
                System.err.println("Unknown button requested");
                System.exit(-1);
                break;
        }
        return (Button) findViewById(R.id.button_tl);
    }


    void endGame(Game ttt) {
        int winner = (ttt.CheckWinner()+3)%3;
        openPopup(winner);

        for(int i=0; i<9; i++){
            ttt.resetPosition(i);
            buttonswitch(i).setBackgroundResource(R.drawable.box);

            ((Button) findViewById(R.id.xbutt)).setVisibility(View.VISIBLE);
            ((Button) findViewById(R.id.obutt)).setVisibility(View.VISIBLE);
        }

    }

    void openPopup(int winner){
        Intent myintent = new Intent(TicTacToe.this,popup.class);
        myintent.putExtra("winner",winner);
        TicTacToe.this.startActivity(myintent);
    }

}
