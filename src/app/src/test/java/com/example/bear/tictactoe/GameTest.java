package com.example.bear.tictactoe;

import org.junit.Test;

import static org.junit.Assert.*;

public class GameTest {
    @Test
    public void checkWinner() throws Exception {
        Game testgame = new Game();
        int output;

        output = testgame.CheckWinner();
        assertEquals(output,0);

        testgame.State[2][0] = -1;
        testgame.State[1][1] = -1;
        testgame.State[0][2] = -1;
        output = testgame.CheckWinner();
        assertEquals(output,-1);

        testgame.State[1][0] = 1;
        testgame.State[1][1] = 1;
        testgame.State[1][2] = 1;
        output = testgame.CheckWinner();
        assertEquals(output,1);

        testgame.State[0][0] = -1;
        testgame.State[0][1] = -1;
        testgame.State[0][2] = 1;
        testgame.State[1][0] = 1;
        testgame.State[1][1] = 1;
        testgame.State[1][2] = -1;
        testgame.State[2][0] = -1;
        testgame.State[2][1] = 1;
        testgame.State[2][2] = 1;
        output = testgame.CheckWinner();
        assertEquals(output,0);
    }

    @Test
    public void makeMove() throws Exception {
        int output;
        Game testgame = new Game();
        testgame.player=-1;
        testgame.State[0][2] = 1;

        testgame.MakeMove(2);
        output = testgame.State[0][2];
        assertEquals(output,1);

        testgame.MakeMove(1);
        output = testgame.State[0][1];
        assertEquals(output, -1);

        output = testgame.State[1][1];
        assertEquals(output, 0);

    }

    @Test
    public void getState() throws Exception {
        int output;
        Game testgame = new Game();
        testgame.player=-1;
        testgame.State[0][2] = 1;
        testgame.State[0][1] = -1;

        output = testgame.getState(2);
        assertEquals(output,1);

        output = testgame.getState(1);
        assertEquals(output, -1);

        output = testgame.getState(4);
        assertEquals(output, 0);
    }

    @Test
    public void copyArray() throws Exception {
        Game testgame = new Game();
        int[][] testFromArray = {{1,2,3},{4,5,6},{7,8,9}};
        int[][] testToArray = new int[3][3];

        testgame.copyArray(testFromArray,testToArray);
        assertArrayEquals(testFromArray,testToArray);
    }

    @Test
    public void getComputerMove() throws Exception {
        Game testgame = new Game();
        testgame.player = -1;
        int output;

        int [][] originalArray={{1,-1,1},{-1,-1,0},{1,1,0}};
        testgame.copyArray(originalArray,testgame.State);
        output = testgame.getComputerMove();
        assertEquals(output,5);
        assertArrayEquals(testgame.State,originalArray);

        int [][] testArray={{-1,0,1},{0,1,0},{0,0,0}};
        testgame.copyArray(testArray,testgame.State);
        output = testgame.getComputerMove();
        assertEquals(output,6);

        testgame.player = 1;
        int [][] testArray2={{-1,0,1},{0,1,0},{0,0,0}};
        testgame.copyArray(testArray2,testgame.State);
        output = testgame.getComputerMove();
        assertEquals(output,1);

    }

    @Test
    public void resetPosition() throws Exception {
        Game testgame = new Game();
        testgame.State[2][0] = 1;
        testgame.resetPosition(6);

        int output = testgame.State[2][0];
        assertEquals(output, 0 );
    }

    @Test
    public void checkGameOver() throws Exception {
        Game testgame = new Game();
        boolean output;

        output = testgame.CheckGameOver();
        assertEquals(output,false);

        testgame.State[2][0] = -1;
        testgame.State[1][1] = -1;
        testgame.State[0][2] = -1;
        output = testgame.CheckGameOver();
        assertEquals(output,true);

        testgame.State[1][0] = 1;
        testgame.State[1][1] = 1;
        testgame.State[1][2] = 1;
        output = testgame.CheckGameOver();
        assertEquals(output,true);

        testgame.State[0][0] = -1;
        testgame.State[0][1] = -1;
        testgame.State[0][2] = 1;
        testgame.State[1][0] = 1;
        testgame.State[1][1] = 1;
        testgame.State[1][2] = -1;
        testgame.State[2][0] = -1;
        testgame.State[2][1] = 1;
        testgame.State[2][2] = 1;
        output = testgame.CheckGameOver();
        assertEquals(output,true);

        testgame.State[2][2] = 0;
        output = testgame.CheckGameOver();
        assertEquals(output,false);
    }

}