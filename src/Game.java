import java.util.Scanner;

class Game {

	int[][] State; // Java implicitly defaults integer arrays to zero
	char[][] map; // This is the display map
	int player; // 1 or -1
	int winner;
	int totalMoves = 0;
	Scanner input = new Scanner(System.in);

	Game(Game g2) {
		State = new int[3][3];
		map = new char[3][3];
		copyArray(g2.State, this.State);
		copyArray(g2.map, this.map);
		this.player = g2.player;
		this.winner = g2.winner;
	}

	Game() {
		player = 1;
		State = new int[3][3];
		map = new char[3][3];
		map[0][0] = ' ';
		map[0][1] = ' ';
		map[0][2] = ' ';
		map[1][0] = ' ';
		map[1][1] = ' ';
		map[1][2] = ' ';
		map[2][0] = ' ';
		map[2][1] = ' ';
		map[2][2] = ' ';
	}

	int CheckWinner() {
		if (State[0][0] + State[0][1] + State[0][2] == 3)
			return 1;
		if (State[1][0] + State[1][1] + State[1][2] == 3)
			return 1;
		if (State[2][0] + State[2][1] + State[2][2] == 3)
			return 1;
		if (State[0][0] + State[1][0] + State[2][0] == 3)
			return 1;
		if (State[0][1] + State[1][1] + State[2][1] == 3)
			return 1;
		if (State[0][2] + State[1][2] + State[2][2] == 3)
			return 1;
		if (State[0][0] + State[1][1] + State[2][2] == 3)
			return 1;
		if (State[0][2] + State[1][1] + State[2][0] == 3)
			return 1;
		if (State[0][0] + State[0][1] + State[0][2] == -3)
			return -1;
		if (State[1][0] + State[1][1] + State[1][2] == -3)
			return -1;
		if (State[2][0] + State[2][1] + State[2][2] == -3)
			return -1;
		if (State[0][0] + State[1][0] + State[2][0] == -3)
			return -1;
		if (State[0][1] + State[1][1] + State[2][1] == -3)
			return -1;
		if (State[0][2] + State[1][2] + State[2][2] == -3)
			return -1;
		if (State[0][0] + State[1][1] + State[2][2] == -3)
			return -1;
		if (State[0][2] + State[1][1] + State[2][0] == -3)
			return -1;
		return 0; // The cat wins
	}

	void Display() {

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (State[i][j] == 1)
					map[i][j] = 'x';
				if (State[i][j] == -1)
					map[i][j] = 'o';
				if (State[i][j] == 0)
					map[i][j] = ' ';
			}
		}
		System.out.printf("\n%c|%c|%c\n", map[0][0], map[0][1], map[0][2]);
		System.out.printf("-+-+-\n");
		System.out.printf("%c|%c|%c\n", map[1][0], map[1][1], map[1][2]);
		System.out.printf("-+-+-\n");
		System.out.printf("%c|%c|%c\n", map[2][0], map[2][1], map[2][2]);
	}

	int getPlayerMove() {
		int x = input.nextInt();
		int y = input.nextInt();
		return 3 * x + y;
	}

	void MakeMove(int position) {
		int y = position % 3;
		int x = (position - y) / 3;
		if (State[x][y] == 0) {
			State[x][y] = player;
			totalMoves++;
		}
	}

	int getState(int position) {
		int y = position % 3;
		int x = (position - y) / 3;
		return State[x][y];
	}

	void endGame(int winner) {
		Display();
		if (winner == 0) {
			System.out.printf("The cat wins!");
		} else {
			System.out.printf("Player %d wins!", (winner + 3) % 3);
		}
		input.close();
	}

	void copyArray(int[][] arrFrom, int[][] arrTo) {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				arrTo[i][j] = arrFrom[i][j];
	}

	void copyArray(char[][] arrFrom, char[][] arrTo) {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				arrTo[i][j] = arrFrom[i][j];
	}

	int getComputerMove() {
		return minimax.mnmx(this)[1];
	}

	int resetPosition(int position) {
		int y = position % 3;
		int x = (position - y) / 3;
		State[x][y] = 0;
		if (State[x][y] != 0) {
			totalMoves--;
			return 1;
		} else {
			return 0;
		}
	}

	boolean CheckGameOver() {
		if (State[0][0] != 0)
			if (State[0][1] != 0)
				if (State[0][2] != 0)
					if (State[1][0] != 0)
						if (State[1][1] != 0)
							if (State[1][2] != 0)
								if (State[2][0] != 0)
									if (State[2][1] != 0)
										if (State[2][2] != 0)
											return true;
		if (CheckWinner() != 0)
			return true;
		return false;
	}
}
