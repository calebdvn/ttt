class TicTacToe {
	public static void main(String[] args) {
		Game ttt = new Game();
		int winner = 0;
		int position = 0;
		boolean gameover = false;

		while (gameover == false) {
			ttt.Display(); // Display the board
			if (ttt.player == 1) {
				position = ttt.getPlayerMove(); // Get the current player's input
			} else {
				position = ttt.getComputerMove();
			}
			ttt.MakeMove(position); // Apply the selection
			gameover = ttt.CheckGameOver();
			ttt.player = -ttt.player; // Pass the turn to the other player
		}
		ttt.Display(); // Display the board
		winner = ttt.CheckWinner(); // See who won
		ttt.endGame(winner);
	}
}
